# #100DaysOfCode Log - Mel Round 1 

The log of my #100DaysOfCode challenge. Started on June 18, Monday, 2018.
tbh: I skip some days because I code for a living and sometimes I don't want to get home and do more of the same - but I want to try and also grow!

### R1 D1 - June 18, 2018
Continuing full-stack React + Node.js Udemy course - set up the oauth2 and did basic routing. App lives here but I didn't push these latest changes:
https://powerful-lowlands-56839.herokuapp.com/
I also set up sourcetree+bitbucket locally to push challenge's log!

### R1 D1 2nd try - Restarted June 27, 2018
Struggled to schedule and helped organize a local emergency rally so I couldn't find the time. Restarting today!

Worked through a few JS algorithm exercises from free codecamp for quick practice. Making me aware that while I can think of solutions quickly, the details miss me sometimes. Me hace ver how relevant test-driven development is.

### R1 D2 June 28th 2018
Coninuing with some JS algorithms exercises. Realizing I should be well past this and it's a little too easy so maybe I should move on to something more challenging?

You can see FreeCodeCamp progress here - https://www.freecodecamp.org/guanabanawave

### R1 D? June 29th - July 4 2018
I worked a bit with same free code camp exercises, but not to the degree I need. I'm not giving up though and continuing this project

### R1 D3 Thursday, July 5 2018
Finished setting up OAut20 on my Node.js and troubleshooting - updated authorized callback URLs in the googleAPI and fixed bugs caused by spelling mistakes LOL I should know better!

### R1 D4 Saturday, July 7 2018
Stil had a bit to set up for oauth (one more route to handle once the code from oauth was receive), used passport to authenticate and get the profile, use nodemon to automate server restart when files are updated, and refactored the code.
Learned while refactoring that you can call a 'require' statement as a function and pass an object as parameter (if you use module.exports)
Working app:https://powerful-lowlands-56839.herokuapp.com/

### R1 D5 Sunday, July 8
Still having issues with the bounce challenge for freecodecamp. A bit frustrating but will keep trying...

### R1 D6 Monday, July 9
Went over specifics of authentication once the login info/profile is received. As well as MongodB intro.
Worked a bit on the javascript issues at work but didn't have enough time

### R1 D7 Tuesday, July 10
Set up mongo db database -remotely hosted. mlab.com

### R1 D8 Monday, July 16
Finally figured out the javascript algorigthm that removes all boolean-y values. Can't believe it took me this long! But pasted it on my jsfiddle for perusal

### R1 D9 Tuesday, July 17
Counting work as I am learning a new architecture to program in that uses Entity Framework and Context + Repository + Service model. Uses interfaces a lot too.
*Context* - All data set up and basic database actions  
*Repository* - Where the data models go into  
*Service* - Where the code for data processing actually goes in

### R1 D9_2 Thursday, July 19
Haven't been doing so great with keeping up with daily work. Mostly out of working here and needing a break from all coding. But really want to keep this going.

### R1 D10 Friday, July 20
Figured out one of the algorigthms in FreeCodecamp but for some reason the tests won't pass - even tho the results are as expected. Maybe a bug within it.
My algorithm available [here](https://jsfiddle.net/enelmar/szbnptkr/)

Also set up User js model with mongoose, learning that you have to set up Schema before using it (thereby not being as flexible as using straight up Mongo Db), and a way to use the determined "user" model.

### R1 D11 Monday, July 23
Figured out a SQL query at work that uses "WITH" - two or more diff query results to create a new query!
This was to fix data in a table that was normalized into two diff tables.
Code found [aqui](https://bitbucket.org/empanadx/learningsql/src/master/Lessons%20at%20work/With_query.sql)

### R1 D12 Monday, Aug 6
Learning about pattern at work in knockout.js that marks a model as "changed" if any properties have been changed by the user,
using suscribers

[Article](http://www.knockmeout.net/2011/05/creating-smart-dirty-flag-in-knockoutjs.html)

### R1 D13 Tuesday, Aug 7
Finished up another script and accidentally deleted account lmao

[Codepen for solution](https://codepen.io/tinskanix/pen/yqRbaP)

### R1 D14 Wednesday, Aug 8
Finished up penultimate JS in basic algorithm scripting!
Learned that "break" isn't an actual thing in Javascript, you need to throw a BreakException

[Codepen for solution](https://codepen.io/tinskanix/pen/MBPmer)

### R1 D15 TuesdayAug 14
Finished up last JS algorithm scripting challenge! Super easy, and realizing i need to step it up!

[Codepen]()

### R1 D15_2 Thursday, Aug 16
Added code to my [Bitbucket](https://bitbucket.org/empanadx/freecodeschool/src/master/) for ease of access (private repository for now)

### R1 D16-D20 Tuesday, Aug 21 - 24
Had time to work on a few O-O js exercises in FreeCodeCamp
learning about prototypes and their purpose, constructors, and using typeof prototype to get object types
[FreeCodeCamp profile](https://www.freecodecamp.org/guanabanawave)

### R1 D21 Monday, Aug 27
Went through a few more exercises in FreeCodeCamp for O-O design. 
These are more lessons so I'm doing them as a review and [keeping notes](https://bitbucket.org/empanadx/codelearningnotas/src/master/Design%20Patterns/O-O%20Design%20with%20JS.md)
Also mi [FreeCodeCamp profile](https://www.freecodecamp.org/guanabanawave)