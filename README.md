# I've joined the #100DaysOfCode Challenge

## Logs
[Log 2nd Try - Map out](/Segundo/log2_mapout.md)

* [Log R2 - Check out my progress!](/Segundo/log2_r2.md)
* [Log R1, 2nd Try](/Segundo/log2_r1.md)
* [Log R1](/First/log_R1.md)

## Challenge deets
* [Rules](rules.md)
* [FAQ](FAQ.md)
* [Resources](resources.md)
