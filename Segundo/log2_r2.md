# #100DaysOfCode Log - Mel 2nd Try, Round 2

The log of my #100DaysOfCode challenge. Re-started on Monday September 10, 2018.
This second round will be building basic/semi-basic web apps using vue.js and ionic
You can find the log for [round 1 here](https://bitbucket.org/empanadx/a_100daysofcode/src/master/Segundo/log2_r1.md)

## R2 D1 - November 7, 2018
I started setting up the basics: vue-cli, bootstrap-vue, and set up an account with Okta (tho i'm having some authentication issues)
[Source Code - to add](#)   
[Using this article](https://dev.to/oktadev/build-a-basic-crud-app-with-vuejs-and-node-4cl8)

### R1 D2 - November 9, 2018
Taking a quick break to re-review Ionic :). Reviewing some of my old notes [here](https://bitbucket.org/empanadx/codelearningnotas/src/master/Frameworks/Ionic/IonicAngularJS.md).  
[Source Code - to add](#)

### R1 D# - XXXX, 2018
(Explanation of progress)  
[Source Code](#)
