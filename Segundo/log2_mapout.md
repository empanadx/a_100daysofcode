## Round One Map-out
Here is the articles/ideas I plan on using for this first round of my #100DaysofCode challenge. This map-out helps me stay on task and plan ahead

**Automating and Scraping using JS and puppeteer**  
*What?* Scripts using javascript, node, & puppeteer to automate file scraping  
*When?* September 10-19  
[Link](https://codeburst.io/a-guide-to-automating-scraping-the-web-with-javascript-chrome-puppeteer-node-js-b18efb9e9921)  


**Why Web Workers Make JavaScript More Than a Single Thread**  
*What?* Multi-threaded scripting w/JS  
*When?* Sept 24, Oct 3 - 5  
[Link](https://codeburst.io/why-web-workers-make-javascript-more-than-a-single-thread-3d489ffad502)  

**A Beginner's Guide to JavaScript's Prototype**  
*What?*  Semi-deep dive into prototype from JS objects, diff ways of instantiating new objects and also a side-ways intro to inheritance
*When?*  Oct 18-Nov 6  
[Link](https://dev.to/tylermcginnis/a-beginners-guide-to-javascripts-prototype-5kk)  

## Round Two Map-out
**Build a Basic CRUD App with Vue.js and Node **  
*What?*  This tutorial will take you step by step through scaffolding a Vue.js project, offloading secure authentication to Okta's OpenID Connect API (OIDC), locking down protected routes, and performing CRUD operations through a backend REST API server. 
*When?*  Nov 7 - ??  
[Link](https://dev.to/oktadev/build-a-basic-crud-app-with-vuejs-and-node-4cl8)  

## Some Ideas

**Taking Redada Alertas project and working on it**  
*What?*  Celso Mirele's open source project for raid alertas, starting with getting an understanding of the project, taking a look at the issues and seeing how it can be improved
*When?*  Before the end of 2018
[Link](https://github.com/Cosecha/redadalertas/blob/master/CONTRIBUTING.md)  

**Article Title**  
*What?*  
*When?*  
[Link](#)  