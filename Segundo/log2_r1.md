# #100DaysOfCode Log - Mel 2nd Try, Round 1

The log of my #100DaysOfCode challenge. Re-started on Monday September 10, 2018.
Re-started to start clean and for real. I do not do 1hr everyday since my day job involves coding and being in front of a computer 8hrs a day,\
but want to make it a habit to practice and/or learn something new everyday and be able to show it.
You can find the log for [attempt 1 here](https://bitbucket.org/empanadx/a_100daysofcode/src/master/log_R1.md)

## Round 1 - JS scripting
Using small tutorials from articles that deal with scripting the web using js
**First one is [Automating and Scraping using JS and puppeteer](https://codeburst.io/a-guide-to-automating-scraping-the-web-with-javascript-chrome-puppeteer-node-js-b18efb9e9921) as reference for this first round!**

### R1 D1 - September 10, 2018
Went trough first example using async function from puppeteer to take a screenshot of the browser  
[Source Code](https://bitbucket.org/empanadx/100daysofcode_code/src/AutomatedScripting/test.js)

### R1 D2 - September 11, 2018
Started scrape.js with empty shell, and setting up page load. As well as a couple exercises on FreeCodeCamp.  
[scrape source code](https://bitbucket.org/empanadx/100daysofcode_code/src/AutomatedScripting/scrape.js) |
[js notes](https://bitbucket.org/empanadx/CodeLearningNotas/Design Patterns/O-O Design with JS.md)

### R1 D3 - September 12, 2018
Finished example 2 - setting up script to open a page and scrape a single record, using document.querySelector  
[scrape source commit](https://bitbucket.org/empanadx/100daysofcode_code/commits/decee964dea4ad2e061579aedf72a5c13c33f5f9)

### R1 D4 - September 13, 2018
Finished tutorial - and mostly figured out on my own how to pull all records in a single page, using document.querySelectorAll. 
Learned you can user this method to select by class name. Digging through childNodes in Console, let me see where exactly the data was - 
tho I tried to make it more complicated by automating it but after seeing the code, realized that it wasn't necessary.  
[Source Code](https://bitbucket.org/empanadx/100daysofcode_code/src/master/AutomatedScripting/scrape.js)

### R1 D5 - September 19, 2018
Made my own version of a scrape tutorial, but this time scraping from a pinterest board and saving results to a file.  
[Source Code](https://bitbucket.org/empanadx/100daysofcode_code/src/master/AutomatedScripting/scrape_pinterest.js)

### R1 D6 - September 24, 2018
Ran through a quick tutorial on multi-threading with workers in JS  
  [webworkers_main.js](https://bitbucket.org/empanadx/100daysofcode_code/src/master/AutomatedScripting/webworkers_main.js) | 
[webworkers_worker.js](https://bitbucket.org/empanadx/100daysofcode_code/src/master/AutomatedScripting/webworkers_worker.js)

### R1 D7 - October 5, 2018
Read through MDN's note on webworkers which honestly made way more sense. I wrote some [Notes here](https://bitbucket.org/empanadx/codelearningnotas/src/master/Design%20Patterns/Multithreading-with-JS.md)

### R1 D8 - October 11, 2018
Did the coding example for a *dedicated* web worker, with some modifications for workers in JS using same Mozilla walk-through/explanation  
[main script](https://bitbucket.org/empanadx/100daysofcode_code/src/master/AutomatedScripting/mdn_main.js) | 
[worker script](https://bitbucket.org/empanadx/100daysofcode_code/src/master/AutomatedScripting/mdn_worker.js) | 
[html page](https://bitbucket.org/empanadx/100daysofcode_code/src/master/AutomatedScripting/mdn_index.html)

### R1 D9-? - October 18- Nov 2nd, 2018
Slow progress. But I read through this article on Prototypes and it was much better explained than FreeCodeCamp's section on object-oriented programming with javascript  
This article goes a little more in depth into what ``prototype``, ``Object.create``, and the ``this`` keyword do and how it comes together  
[Code Notas](https://bitbucket.org/empanadx/codelearningnotas/src/master/Design%20Patterns/O-O%20Design%20with%20JS.md) |
[Article](https://dev.to/tylermcginnis/a-beginners-guide-to-javascripts-prototype-5kk)


### R1 D# - XXXX, 2018
(Explanation of progress)  
[Source Code](#)
